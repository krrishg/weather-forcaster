package weather.forcaster;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class WeatherForcaster {
    public static void main(String[] args) { 
        String place;
        Scanner in = new Scanner(System.in);
        
        System.out.print("ठाउँ:");
        place = in.nextLine();
        
        try {
            URL url = new URL(
                    "https://nepal-weather-api.herokuapp.com/api/?placenp="+
                            place);
            HttpURLConnection request = (HttpURLConnection) 
                    url.openConnection();
            request.connect();
            
            JSONParser parser = new JSONParser();
            JSONObject obj = (JSONObject) parser.parse(new InputStreamReader((
                    InputStream)request.getContent()));
            String status = (String) obj.get("status");
            
            if (status.equals("true")){
                String max=(String) obj.get("max");
                String min=(String) obj.get("min");
                String rain=(String) obj.get("rain");
            
                System.out.println("अधिक्तम:"+max);
                System.out.println("न्युनतम"+min);
                System.out.println("वर्षा:"+rain);
            } else {
                System.out.println("ठाउँ भेटिएन।उपलब्ध ठाउँहरु:");
                System.out.println("'Dadeldhura','Dipayal','Dhangadi','Birendranagar','Nepalgunj','Jumla','Dang','Pokhara','Bhairahawa','Simara','Kathmandu','Okhaldhunga','Taplejung','Dhankuta','Biratnagar','Jomsom','Dharan','Lumle','Janakpur','Jiri'");
            }
        } catch (IOException | ParseException ex) {
            System.out.println(ex.getMessage());
        }
    }    
}
